# Vocabulary:

In the **command** "3d6+5d4k4+1;1d10", "3d6+5d4k4+1" and "1d10" are **expressions**."3d6", "+5d4", "k4", "+1", and  "1d10" are **instructions**. Each instruction return a list of **value** and other information (like string output, color, max value of a dice, …).


The following operators have no input:

 - d (roll a dice)
 - l (roll a list)

The following operators require to be able to acces the 1st previous values in the current expression:

 - a (Reroll and add)
 - c (Count)
 - e (Explode)
 - g (Group)
 - i (if)
 - k (Keep)
 - kl (Keep lower)
 - r (Reroll)
 - R (Reroll until)
 - s (Sort)

Thoses operators have additionnal requirements:

 - K (Explode and keep): see explode and keep operators.
 - e (Explode): it could be extented to not require a validator (taking the maximum value of the dice as validator), and thus require to access the maximum value of all the g of the 1st previous expression (current behaviour of K).
 - m (Merge): it can access to the values of any of the prevous expressions.
 - p (Paint dice): it can attach a color to a specific dice
 - # (Comment): display a string (distinct from the normal output).
 - ; (Next expression): it can have multiple expressions in one expression
 - @ (Backward Jump): it can access to the 2nd previous values in the current expression. This may be extented to the n-th previous values.
 - \" (output): display an alternate output string

Additionnal operators will have the following requirements:

 - ! (Several expression): We need to be able to dupplicate an expression
 - $ (scalar result: give the scalar output of the n-th previous expression): it can access to the last instruction (or directly the sum if it exist) of any previous expressions.
 - & (rolls: give the values of the last instruction of the n-th previous expression): it can access to the last instruction of any previous expressions.

 I propose the following output structure that should meet all thoses requirements:

 ```
 [ // list of expression (1 or more), must be ordered
    [ // list of instruction (1 or more), must be ordered. When an expression is finished, only the values of last instructions must be kept (for `$` operator) and the dices of all instructions.
        {
            "name": operator-name, // mainly for debug
            "dices": [ // sorted, can be empty, list of dices rolles in the instruction. Currently both "d" and "l" generates only one type of dices, so this array would contains only 1 element if it exist
                { type: {"d": 10}, "values": [3, 4, 5] }, // type is either "d" (with the maximum value) or "l" (with the associated list of values), and "values" is a list of value rolled in the current instruction
                { type: {"l": 1, text:["sword", "arc", "potion", "dagger"]}, "values": [3, 4] } // example of list. The id 1 is unique among all the lists
            ],
            "value": [
                // current values (1 or more)
                // sorted
                // each value must have a type associated because of `e` (explode)
                { type: {"d": 10}, "values": [3, 4, 5] },
                { type: {"l": 1}, "values": [3, 4] }, // example of list. To get the text associated with the values "3" and "4", we must look in the "dices" of the precious instructions until we find the list "l" with the associated id "1".
                { type: {"d": 6}, "values": [1, 1] },
                { type: {"d": 6}, "values": [6], "color":"blue" } // example of a colored dice
            ]
        }
    ]
]

