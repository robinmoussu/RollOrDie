#include <array>
#include <limits>
#include <numeric>
#include <vector>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace std;

namespace print_vector {
    template<class T>
    std::ostream& operator<< (std::ostream& os, const vector<T>& d) {
        os << "[ ";
        for (auto it = cbegin(d); it != cend(d); it++) {
            os << *it;
            if ((it + 1) != cend(d)) {
                os << ", ";
            }
        }
        os << " ]";
        return os;
    }
};

class Dice_t {
public:
    class Values {
        friend Dice_t;
        using Value = int_fast32_t;
    public:
        Values() = default;
        Values(initializer_list<Value> v)
            : vs{v}
        {}
        void operator=(Value v) {
            vs = {v};
        }
        void operator=(Values v) {
            vs = move(v.vs);
        }
        Values operator+(Value v) {
            vs.push_back(v);
            return *this;
        }
        Value sum() const {
            return accumulate(begin(vs), end(vs), 0);
        }
        bool operator<=(const Values& other) const {
            return sum() <= other.sum();
        }
        bool operator==(const Values& other) const {
            return sum() == other.sum();
        }
        friend std::ostream& operator<< (std::ostream& os, const Values v) {
            using namespace print_vector;
            return os << v.vs;
        }
    private:
        vector<Value> vs;
    };

    using Value = Values::Value;

public:
    struct Type {
        char name;
        int_fast32_t id;

    public:
        // TODO: replace with c++20's spaceship operator in 2020!
        bool operator<(const Type& other) const {
            return
                this->name <= other.name &&
                this->id <= other.id;
        }
        bool operator== (const Type& other) const {
            return
                this->name == other.name &&
                this->id == other.id;
        }
        bool operator!= (const Type& other) const {
            return ! (*this == other);
        }
        friend std::ostream& operator<< (std::ostream& os, const Type& t) {
            return os << "{ \"" << t.name << "\": " << t.id << "}";
        }
    };

public:
    Type type;
    Values values;

public:
    Dice_t() = default;
    Dice_t(Type t, Values v)
        : type{t}
        , values{v}
    {}
    Dice_t(Type t, Value v)
        : Dice_t{t, Values{v}}
    {}

public:
    bool operator<(const Dice_t& other) const {
        return
            this->type < other.type &&
            this->values <= other.values;
    }
    bool operator==(const Dice_t& other) const {
        return
            this->type == other.type &&
            this->values == other.values;
    }
    bool operator!= (const Dice_t& other) const {
        return ! (*this == other);
    }
    friend std::ostream& operator<< (std::ostream& os, const Dice_t& d) {
        return os << "{ " << d.type << ", " << d.values << "}";
    }
};

struct Values;
class MockRand;
struct Instruction;
using Dices = vector<Dice_t>;

template<Dice_t::Value max_value, class... ValueType>
Dices Dice(ValueType&&... values);
template <const char*... possible_values, class... ValueType>
Dices List(ValueType&&... values);
template <class... ValueType>
Dices Scalar(ValueType&&... values);
Dices operator+ (Dices d1, Dices d2);

class MockRand {
public:
    MockRand(initializer_list<int_fast32_t> numbers)
        : m_numbers{numbers}
        , m_it{cbegin(m_numbers)}
    {}
    int_fast32_t operator() () {
        assert(m_it != end(m_numbers));
        return *m_it++;
    }
private:
    const vector<int_fast32_t> m_numbers;
    vector<int_fast32_t>::const_iterator m_it;
};

// TODO: find a better name
class Values {
    Values() = default;

    mutable MockRand rand = MockRand{{}}; // TODO: Implement the non-mock version
    Dices dices;

public:
    using validator_t = bool(*)(int_fast32_t value); // TODO: implement a real class, must be able to access any given information, especially the current dice

    Instruction roll(Dice_t::Type t, int_fast32_t number_of_dices) const;
    Instruction add(validator_t validator) const;
    Instruction explode() const; // TODO: add a validator
    Instruction reroll(validator_t validator) const;
    Instruction reroll_until(validator_t validator) const;
    Instruction keep_lower(int_fast32_t dice_to_keep) const;
    Instruction keep(int_fast32_t dice_to_keep) const;
    Instruction count(validator_t validator) const;
    Instruction group(int_fast32_t group_size) const;
    Instruction sort() const;

public:
    Values(Dices d)
        : dices{move(d)}
    {}
    Values(MockRand hardcoded_random_number, Dices d)
        : rand{move(hardcoded_random_number)}
        , dices{move(d)}
    {}
//         Values(Dice_t::Type t, vector<Dice_t::Value> values) :
//             Values{ [&](){
//                 auto value = begin(values);
//                 auto dices = Dices(values.size());
//                 generate(begin(dices), end(dices), [&]{ return Dice_t{t, *value++};});
//                 return move(dices);
//             }()}
//         {}
    bool operator==(const Values& other) const {
        return this->dices == other.dices;
    }
    bool operator!= (const Values& other) const {
        return ! (*this == other);
    }
    friend std::ostream& operator<< (std::ostream& os, const Values& v) {
        using namespace print_vector;
        return os << v.dices;
    }
};

struct Instruction {
    string name;
    Dices dices;
    Values values;

public:
    bool operator==(const Instruction& other) const {
        return
            this->name == other.name &&
            this->dices == other.dices &&
            this->values == other.values;
    }
    bool operator!= (const Instruction& other) const {
        return ! (*this == other);
    }
    friend std::ostream& operator<< (std::ostream& os, const Instruction& i) {
        using namespace print_vector;
        return os << "{ \"" << i.name << "\", " << i.dices << ", " << i.values << " }";
    }

};

template<class DiceType>
Dices generate_dices(DiceType type, initializer_list<Dice_t::Value> values) {
    auto dices = Dices(values.size());
    auto value = begin(values);
    generate(begin(dices), end(dices), [&]{
        return Dice_t{type, *value++};
    });
    return dices;
}

struct Expression {
    vector<Instruction> instructions;

public:
    bool operator==(const Expression& other) const {
        return this->instructions == other.instructions;
    }
    friend std::ostream& operator<< (std::ostream& os, const Expression& e) {
        using namespace print_vector;
        return os << e.instructions;
    }
};


struct Command {
    vector<Expression> expressions;
};

////////////////////////////////////////////////////////////////////////////////

// NB: the use of variadic argument allow to call Dice<10>(5), as well as
// Dice<10>(3,4,5) to generated Dices of 10 faces with value {5} in the first
// case, and 3 dices with value {3,4,5} in the second case.
//
// I could have used initilizer_list witch require an extra pair of curly
// braces Dice<10>({3,4,5}) or the operator elipsis, but I'm not confindent
// with the use of va_start/va_arg/va_end.
template<Dice_t::Value max_value, class... ValueType>
Dices Dice(ValueType&&... values) {
    return generate_dices(Dice_t::Type{'D', max_value}, {values...});
}
template <const char*... possible_values, class... ValueType>
Dices List(ValueType&&... values) {
    return generate_dices(Dice_t::Type{'L', 0/*generate_id(possible_values, ...)*/}, {values...});
}
template <class... ValueType>
Dices Scalar(ValueType&&... values) {
    return generate_dices(Dice_t::Type{'S', 0}, {values...});
}
Dices operator+ (Dices d1, Dices d2)
{
    d1.reserve(d1.size() + d2.size());
    copy(begin(d2), end(d2), back_inserter(d1));
    return d1;
}

Instruction Values::roll(Dice_t::Type t, int_fast32_t number_of_dices) const
{
    auto rolled = Dices(number_of_dices);
    generate(begin(rolled), end(rolled), [&] {
        return Dice_t{t, rand()};
    });

    return Instruction{
        "roll-dice",
        rolled,
        dices + rolled
    };
}

Instruction Values::add(validator_t validator) const
{
    auto rolled = Dices{};
    auto new_dices = dices;
    for (auto& dice: new_dices) {
        auto value = 0;
        if (validator(dice.values.sum())) {
            value = rand();
            rolled.push_back(Dice_t{dice.type, value});
            dice = Dice_t{dice.type, dice.values + value};
        }
    };

    return Instruction{
        "add",
        rolled,
        new_dices
    };
}

Instruction Values::explode() const
{
    auto rolled = Dices{};
    auto new_dices = dices;
    for (auto& dice: new_dices) {
        auto value = dice.values.sum();
        // TODO: make validator an argument
        auto validator = [&] (auto value) { return value == dice.type.id; }; // works only for dice, not list or scalar
        while (validator(value)) {
            value = rand();
            rolled.push_back(Dice_t{dice.type, value});
            dice = Dice_t{dice.type, dice.values + value};
        }
    };

    return Instruction{
        "explode",
        rolled,
        new_dices
    };
}

Instruction Values::reroll(validator_t validator) const
{
    auto rolled = Dices{};
    auto new_dices = dices;
    for (auto& dice: new_dices) {
        auto value = dice.values.sum();
        // TODO: make validator an argument
        if (validator(value)) {
            value = rand();
            rolled.push_back(Dice_t{dice.type, value});
            dice = Dice_t{dice.type, value};
        }
    };

    return Instruction{
        "reroll",
        rolled,
        new_dices
    };
}

Instruction Values::reroll_until(validator_t validator) const
{
    auto rolled = Dices{};
    auto new_dices = dices;
    for (auto& dice: new_dices) {
        auto value = dice.values.sum();
        // TODO: make validator an argument
        while (validator(value)) {
            value = rand();
            rolled.push_back(Dice_t{dice.type, value});
            dice = Dice_t{dice.type, value};
        }
    };

    return Instruction{
        "reroll-until",
        rolled,
        new_dices
    };
}

Instruction Values::keep_lower(int_fast32_t dice_to_keep) const
{
    return Instruction{
        "keep-lower",
        Dices{},
        [&]{
            auto sorted = dices;
            std::sort(begin(sorted), end(sorted));
            return Dices{
                cbegin(sorted),
                min(cend(sorted), cbegin(sorted) + dice_to_keep)
            };
        }()
    };
}
Instruction Values::keep(int_fast32_t dice_to_keep) const
{
    return Instruction{
        "keep",
        Dices{},
        [&]{
            auto sorted = dices;
            std::sort(begin(sorted), end(sorted));
            return Dices{
                std::max(cbegin(sorted), cend(sorted) - dice_to_keep),
                cend(sorted)
            };
        }()
    };
}
Instruction Values::count(Values::validator_t validator) const
{
    return Instruction{
        "count",
        Dices{},
        Scalar(accumulate(begin(dices), end(dices), 0, [&](auto acc, Dice_t d) { return acc + (validator(d.values.sum())? 1 : 0); }))
    };
}

using Vals = vector<Dice_t::Value>;

// create a group at least as big as `group_size` from the elements of `from`
// into `to`, unless the sum of the values in `from` is < `group_size`.  The
// biggest element of `from` are used in case multiple combinaison give the
// same sum
// If a group is created, `it` point to index of th last element + 1 of `to`
// containing a value
void create_group(Vals& from, Vals& to, int_fast32_t& it, int_fast32_t& sum, const int_fast32_t group_size) {
    const auto nb_element = from.size();
    for (; it < nb_element && sum < group_size; ++it) {
        if (from[it]) {
            swap(from[it], to[it]);
            sum += to[it];
        }
    }
};


// replace the smallest value of `to` by one or many value smaller from `from`
void replace_smaller(Vals& base_from, Vals& base_to, int_fast32_t it, int_fast32_t& sum, int_fast32_t best_sum, const int_fast32_t group_size) {
    const auto nb_element = base_from.size();
    auto from = base_from;
    auto to = base_to;
    auto current_sum = sum - to[it];

    swap(from[it], to[it]);
    for (++it;it != nb_element; ++it) {
        if (from[it]) { // we only use non-null element from `from`
            swap(from[it], to[it]);
            current_sum += to[it];
            if (current_sum >= group_size) {
                // a new valid group was found

                auto update = [&] {
                    if (current_sum < best_sum) {
                        base_from = from;
                        base_to = to;
                        sum = current_sum;
                        best_sum = current_sum;
                    }
                };

                update();
                if (sum == group_size) {
                    return; // best possible group found
                }
                // let's try again to use smaler element to replace the new smallest one
                replace_smaller(from, to, it, current_sum, best_sum, group_size);
                update();
                if (sum == group_size) {
                    return; // best possible group found
                }
            }
        }
    }
};

// Create a group (unless the sum of elements in `from` is less than
// `group_size`. It use then biggest elements from `from` until the sum is >=
// group_size, then it try (recursively) to replace the smallest element
// selected by one or more smaller element in `from`. Only the group that
// contains the smallest sum is kept.
bool left_to_right(Vals& from, Vals& to, int_fast32_t it, int_fast32_t& sum, const int_fast32_t group_size) {
    create_group(from, to, it, sum, group_size);
    if (sum < group_size) {
        return false;
    }
    if (sum == group_size) {
        return true;
    }

    replace_smaller(from, to, it-1, sum, sum, group_size);
    return true;
};

// Try to replace the element of `to` from smaller elements of `from` to create
// a smaller sum, walking right to left until `left_index_stop` is reach.
void right_to_left(Vals& base_from, Vals& base_to, int_fast32_t left_index_stop, int_fast32_t& sum, int_fast32_t best_sum, const int_fast32_t group_size) {
    const auto nb_element = base_from.size();

    for(auto it = nb_element - 1; it > left_index_stop; --it) {
        auto from = base_from;
        auto to = base_to;
        // first swap back all elements of `to` into `from`
        auto current_sum = sum;
        bool skip_me = true;
        for (auto it_clean = it; it_clean < nb_element; ++it_clean) {
            if (to[it_clean]) {
                skip_me = false;
                current_sum -= to[it_clean];
                swap(from[it_clean], to[it_clean]);
            }
        }
        if (skip_me) {
            continue;
        }
        if (left_to_right(from, to, it+1, current_sum, group_size)) {
            auto update = [&] {
                if (current_sum < best_sum) {
                    // we found a better group
                    base_from = from;
                    base_to = to;
                    sum = current_sum;
                    best_sum = current_sum;
                }
            };

            update();
            if (sum == group_size) {
                return;
            }
            right_to_left(from, to, it, current_sum, best_sum, group_size);
            update();
            if (sum == group_size) {
                return;
            }
        }
    }
}

Instruction Values::group(int_fast32_t group_size) const
{
    return Instruction{
        "group",
        Dices{},
        [&] {
            return Scalar([&]{
                auto nb_elements = dices.size();
                auto sorted_values = vector<Dice_t::Value>(nb_elements);
                auto it = cbegin(dices);
                generate(begin(sorted_values), end(sorted_values), [&]{ auto ret = it->values.sum(); ++it; return ret; });
                std::sort(begin(sorted_values), end(sorted_values), std::greater<Dice_t::Value>());

                auto nb_group = 0;
                for(;;) {
                    auto new_group = Vals(nb_elements);

                    int_fast32_t sum = 0;
                    if(!left_to_right(sorted_values, new_group, 0, sum, group_size)) {
                        return nb_group;
                    }
                    nb_group++;
                    if (sum != group_size) {
                        right_to_left(sorted_values, new_group, 0, sum, sum, group_size);
                    }
                }
                return nb_group;
            }());
        }()
    };
}
Instruction Values::sort() const
{
    return Instruction{
        "sort",
        Dices{},
        [&] {
            auto sorted_dices = dices;
            std::sort(begin(sorted_dices),end(sorted_dices));
            return sorted_dices;
        }()
    };
}

////////////////////////////////////////////////////////////////////////////////

TEST_CASE("unsorted","instruction")
{
    CHECK(Dice<10>(7,3,4,5,10) !=
          Dice<10>(3,5,4,7,10));
}

TEST_CASE("roll-dice","operator")
{
    // need a way to mock up random dice-rolling
    CHECK(
        Values{MockRand{1,2,3,4,5},{}}.roll(Dice_t::Type{'D',10},5) ==
        Instruction{
            "roll-dice",
            Dice<10>(1,2,3,4,5),
            Dice<10>(1,2,3,4,5)
        });
    CHECK(
        Values{MockRand{1,2,3,4,5},Dice<10>(6,7,8,9,10)}.roll(Dice_t::Type{'D',10},5) ==
        Instruction{
            "roll-dice",
            Dice<10>(1,2,3,4,5),
            Dice<10>(6,7,8,9,10,1,2,3,4,5)
        });
    CHECK(
        Values{MockRand{1,2,3,4,5},Dice<10>(1,2,3,4,5)}.roll(Dice_t::Type{'D',10},5) ==
        Instruction{
            "roll-dice",
            Dice<10>(1,2,3,4,5),
            Dice<10>(1,2,3,4,5,1,2,3,4,5)
        });
    CHECK(
        Values{MockRand{1,2,3,4,5},Dice<6>(1,2,3,4,5)}.roll(Dice_t::Type{'D',10},5) ==
        Instruction{
            "roll-dice",
            Dice<10>(1,2,3,4,5),
            Dice<6>(1,2,3,4,5) + Dice<10>(1,2,3,4,5)
        });
    CHECK(
        Values{MockRand{5,6,1},Dice<6>(1,2) + Dice<10>(1,2,3,10)}.roll(Dice_t::Type{'D',6},3) ==
        Instruction{
            "roll-dice",
            Dice<6>(5,6,1),
            Dice<6>(1,2) + Dice<10>(1,2,3,10) + Dice<6>(5,6,1)
        });
}

TEST_CASE("roll-list","operator")
{
    // need a way to mock up random dice-rolling
}

TEST_CASE("add","operator")
{
    CHECK(
        Values{MockRand{6},Dice<10>(3,4,5,7,10)}.add([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "add",
            Dice<10>(6),
            Dice<10>(3,10,5,7,10)
        });
    CHECK(
        Values{MockRand{},Dice<10>(3)}.add([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "add",
            Dices{},
            Dice<10>(3),
        });
    CHECK(
        Values{MockRand{7,8,9},Dice<10>(3,4,4,4,10)}.add([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "add",
            Dice<10>(7,8,9),
            Dice<10>(3,11,12,13,10),
        });
    CHECK(
        Values{MockRand{4,7,8},Dice<10>(3,4,4,4,10)}.add([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "add",
            Dice<10>(4,7,8),
            Dice<10>(3,8,11,12,10),
        });
    CHECK(
        Values{MockRand{5,6,1,7},Dice<6>(1,2) + Dice<10>(1,2,3,10)}.add([](int_fast32_t val){ return val < 3; }) ==
        Instruction{
            "add",
            Dice<6>(5,6) + Dice<10>(1,7),
            Dice<6>(6,8) + Dice<10>(2,9,3,10)
        });
}

TEST_CASE("explode","operator")
{
    CHECK(
        Values{MockRand{}, Dice<10>(3,4,5,7,8)}.explode() ==
        Instruction{
            "explode",
            Dices{},
            Values{Dice<10>(3,4,5,7,8)}
        });
    CHECK(
        Values{MockRand{5}, Dice<10>(3,4,5,7,10)}.explode() ==
        Instruction{
            "explode",
            Dice<10>(5),
            Values{Dice<10>(3,4,5,7,15)}
        });
    CHECK(
        Values{MockRand{10,5}, Dice<10>(3,4,5,7,10)}.explode() ==
        Instruction{
            "explode",
            Dice<10>(10,5),
            Values{Dice<10>(3,4,5,7,25)}
        });
    CHECK(
        Values{MockRand{10,5,3,3,1}, Dice<10>(3,4,5,7,10) + Dice<3>(1,1,3) + Dice<6>(4,3,5)}.explode() ==
        Instruction{
            "explode",
            Dice<10>(10,5) + Dice<3>(3,3,1),
            Values{Dice<10>(3,4,5,7,25) + Dice<3>(1,1,10) + Dice<6>(4,3,5)}
        });
}

TEST_CASE("reroll","operator")
{
    CHECK(
        Values{MockRand{6},Dice<10>(3,4,5,7,10)}.reroll([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "reroll",
            Dice<10>(6),
            Dice<10>(3,6,5,7,10)
        });
    CHECK(
        Values{MockRand{},Dice<10>(3)}.reroll([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "reroll",
            Dices{},
            Dice<10>(3),
        });
    CHECK(
        Values{MockRand{7,8,9},Dice<10>(3,4,4,4,10)}.reroll([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "reroll",
            Dice<10>(7,8,9),
            Dice<10>(3,7,8,9,10),
        });
    CHECK(
        Values{MockRand{4,7,8},Dice<10>(3,4,4,4,10)}.reroll([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "reroll",
            Dice<10>(4,7,8),
            Dice<10>(3,4,7,8,10),
        });
    CHECK(
        Values{MockRand{5,6,1,7},Dice<6>(1,2) + Dice<10>(1,2,3,10)}.reroll([](int_fast32_t val){ return val < 3; }) ==
        Instruction{
            "reroll",
            Dice<6>(5,6) + Dice<10>(1,7),
            Dice<6>(5,6) + Dice<10>(1,7,3,10)
        });
}

TEST_CASE("reroll-until","operator")
{
    CHECK(
        Values{MockRand{6},Dice<10>(3,4,5,7,10)}.reroll_until([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "reroll-until",
            Dice<10>(6),
            Dice<10>(3,6,5,7,10)
        });
    CHECK(
        Values{MockRand{},Dice<10>(3)}.reroll_until([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "reroll-until",
            Dices{},
            Dice<10>(3),
        });
    CHECK(
        Values{MockRand{7,8,9},Dice<10>(3,4,4,4,10)}.reroll_until([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "reroll-until",
            Dice<10>(7,8,9),
            Dice<10>(3,7,8,9,10),
        });
    CHECK(
        Values{MockRand{4,5,7,8},Dice<10>(3,4,4,4,10)}.reroll_until([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "reroll-until",
            Dice<10>(4,5,7,8),
            Dice<10>(3,5,7,8,10),
        });
    CHECK(
        Values{MockRand{5,2,6,1,7,8},Dice<6>(1,2) + Dice<10>(1,2,3,10)}.reroll_until([](int_fast32_t val){ return val < 3; }) ==
        Instruction{
            "reroll-until",
            Dice<6>(5,2,6) + Dice<10>(1,7,8),
            Dice<6>(5,6) + Dice<10>(7,8,3,10)
        });
}

TEST_CASE("if","operator")
{
    // need validator
}

TEST_CASE("count","operator")
{
    // need validator
    CHECK(
        Values{Dice<10>(3,4,5,7,10)}.count([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "count",
            Dices{},
            Scalar(1)
        });
    CHECK(
        Values{Dice<10>(3)}.count([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "count",
            Dices{},
            Scalar(0)
        });
    CHECK(
        Values{Dice<10>(3,4,4,4,10)}.count([](int_fast32_t val){ return val == 4; }) ==
        Instruction{
            "count",
            Dices{},
            Scalar(3)
        });
}

TEST_CASE("keep-lower","operator")
{
    CHECK(
        Values{Dice<10>(3,4,5,7,10)}.keep_lower(3) ==
        Instruction{
            "keep-lower",
            Dices{},
            Values{Dice<10>(3,4,5)}
        });
    CHECK(
        Values{Dice<10>(9,4,3,7,10)}.keep_lower(3) ==
        Instruction{
            "keep-lower",
            Dices{},
            Values{Dice<10>(3,4,7)}
        });
    CHECK(
        Values{Dice<10>(3)}.keep_lower(3) ==
        Instruction{
            "keep-lower",
            Dices{},
            Values{Dice<10>(3)}
        });
    CHECK(
        Values{Dice<10>(3,4,4,4,10)}.keep_lower(3) ==
        Instruction{
            "keep-lower",
            Dices{},
            Values{Dice<10>(3,4,4)}
        });
}

TEST_CASE("keep","operator")
{
    CHECK(
        Values{Dice<10>(3,4,5,7,10)}.keep(3) ==
        Instruction{
            "keep",
            Dices{},
            Values{Dice<10>(5,7,10)}
        });
    CHECK(
        Values{Dice<10>(9,4,3,7,10)}.keep(3) ==
        Instruction{
            "keep",
            Dices{},
            Values{Dice<10>(7,9,10)}
        });
    CHECK(
        Values{Dice<10>(2)}.keep(3) ==
        Instruction{
            "keep",
            Dices{},
            Values{Dice<10>(2)}
        });
    CHECK(
        Values{Dice<10>(3,4,4,4,10)}.keep(3) ==
        Instruction{
            "keep",
            Dices{},
            Values{Dice<10>(4,4,10)}
        });
}

TEST_CASE("group","operator")
{
    CHECK(
        Values{Dice<10>(3,4,6,1,2)}.group(7) ==
        Instruction{
            "group",
            Dices{},
            Scalar(2)
        });
    CHECK(
        Values{Dice<10>(9,9,2)}.group(10) ==
        Instruction{
            "group",
            Dices{},
            Scalar(1)
        });
    CHECK(
        Values{Dice<10>(9,9,2,2)}.group(10) ==
        Instruction{
            "group",
            Dices{},
            Scalar(2)
        });
    CHECK(
        Values{Dice<10>(9,2,9,2)}.group(10) ==
        Instruction{
            "group",
            Dices{},
            Scalar(2)
        });
    CHECK(
        Values{Dice<10>(7,4,3,6)}.group(10) ==
        Instruction{
            "group",
            Dices{},
            Scalar(2)
        });
    CHECK(
        Values{Dice<10>(8,4,3,9)}.group(10) ==
        Instruction{
            "group",
            Dices{},
            Scalar(2)
        });
    CHECK(
        Values{Dice<100>(34,33,33,26,25,25,24,21,20,20,20,19)}.group(100) ==
        Instruction{
            "group",
            Dices{},
            Scalar(3)
        });
    CHECK(
        Values{Dice<100>(50,25,25,33,34,32,1,10,11,12,13,54)}.group(100) ==
        Instruction{
            "group",
            Dices{},
            Scalar(3)
        });
    CHECK(
        Values{Dice<100>(50,28,25,33,34,32,5,9,11,12,13,56)}.group(100) ==
        Instruction{
            "group",
            Dices{},
            Scalar(3)
        });
    CHECK(
        Values{Dice<100>(50,51,52,53,54,55,56,57,58,59)}.group(1) ==
        Instruction{
            "group",
            Dices{},
            Scalar(10)
        });
}


TEST_CASE("sort","operator")
{
    CHECK(Values{Dice<10>(7,3,4,5,10)}.sort() ==
        Instruction{
            "sort",
            Dices{},
          Values{Dice<10>(3,4,5,7,10)}
      });
}


